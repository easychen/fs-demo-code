<?php
// 接口
// 猫娘认证


interface NekoGirls
{
    public function talk(); // 会说话
    public function walk(); // 直立行走
}

abstract class Character
{
    protected $name = 'NoNAME';
    protected $hp = 100;
    protected $mp = 100;
    private $version = 0.1;

    public function talk()
    {
        echo "I am $this->name";
    }
}

class NPC extends Character{};
class NekoGirl extends NPC implements NekoGirls
{
    public function talk()
    {
        parent::talk() ;
        echo "喵\r\n";
    }

    public function walk()
    {
       $this->posx += 5;
       $this->posy += 5;
    }
}

$🐱 = new NekoGirl();
$🐱->talk();

interface NekoGirlsV2 extends NekoGirls
{
    public function work(); // 能干活
}

class NekoGirlV2 extends NekoGirl implements NekoGirlsV2
{
    public function work()
    {
        // sleep
        echo "👩🏻‍🍳👩🏻‍🌾";
    }

    public function talk()
    {
      $this->name = '会干活的猫娘A';
      parent::talk();  
    }
}

$🐱Lv2 = new NekoGirlV2();
$🐱Lv2->talk();

function working( NekoGirlV2 $neko )
{
    $neko->work();
}

working( $🐱Lv2 );

