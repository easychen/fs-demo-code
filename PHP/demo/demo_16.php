<?php
include_once "_function.php";

/*
chr — 返回指定的字符
ord — 返回字符的 ASCII 码值
*/

my_echo( ord("A") . chr(65) );


// addslashes — 使用反斜线引用字符串
// stripslashes — 反引用一个引用字符串
$path = "C:\Users\EasyC\Book";
my_echo( addslashes( $path ) );
my_echo( stripslashes( addslashes( $path ) ) );

// HTML 实体转换
// HTML 实体是 HTML 中的特殊字符，HTML4.0 供定义了 252 个实体字符 https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references
// htmlspecialchars_decode — 将特殊的 HTML 实体转换回普通字符
// htmlspecialchars — 将特殊字符转换为 HTML 实体
$html = "<html>
<head></head>
<body>a & b 
<p>Easy's book ♦</p>
<textarea/></body>
</html>";
my_echo( htmlspecialchars( $html ) ); // & 双引号 单引号 大于和小于
my_echo( htmlentities( $html ) ); // 所有 HTML 实体 

// 这里要查看demo_16 HTML 页面源代码才能看见 view-source:http://o.ftqq.com/demo_16.php

// strip_tags — 从字符串中去除 HTML 和 PHP 标记
my_echo( strip_tags( $html ) );
my_echo( strip_tags( $html , '<body><p>' ) );//参数只需要写标签名


// lcfirst — 使一个字符串的第一个字符小写
// ucfirst — 将字符串的首字母转换为大写
// ucwords — 将字符串中每个单词的首字母转换为大写
my_echo( ucfirst( "today is rainy day" ) );
my_echo( ucwords( "today is rainy day" ) );

// strtolower — 将字符串转化为小写
// strtoupper — 将字符串转化为大写
my_echo( strtoupper( "today is rainy day" ) );

// trim — 去除字符串首尾处的空白字符（或者其他字符）
// ltrim — 删除字符串开头的空白字符（或其他字符）
// rtrim — 删除字符串末端的空白字符（或者其他字符）
// chop — rtrim 的别名
my_echo( trim( "today is rainy day" , "ty" ) );
my_echo( rtrim( "/data/book/" , "/" ) );

// explode — 使用一个字符串分割另一个字符串
// implode — 将一个一维数组的值转化为字符串
// join — 别名 implode
$array = [ "a" , "b" , "c" ];
my_echo( join( "-" , $array ) );
my_echo( explode( '/' , '/data/book/howtojump.txt' ) );

// str_split — 将字符串转换为数组
my_echo( str_split( '/data/book/howtojump.txt' , 1 ) );
my_echo( str_split( '/data/book/howtojump.txt' , 3 ) );

// strrev — 反转字符串
my_echo( strrev( 'Easy' ) );

// md5_file — 计算指定文件的 MD5 散列值
// md5 — 计算字符串的 MD5 散列值
// sha1_file — 计算文件的 sha1 散列值
// sha1 — 计算字符串的 sha1 散列值

my_echo( md5( '中文Key' ) );

// echo — 输出一个或多个字符串
// print — 输出字符串
echo "🙄🙄🙄🙄\r\n";
print "🙄🙄🙄🙄\r\n";

// printf — 输出格式化字符串
// 格式参考 http://php.net/manual/zh/function.sprintf.php
printf( " %.02f%% \r\n" , 67.1230023 );
// sprintf — Return a formatted string
// vfprintf — 将格式化字符串写入流
// vprintf — 输出格式化字符串
// vsprintf — 返回格式化字符串


// number_format — 以千位分隔符方式格式化一个数字
// nl2br — 在字符串所有新行之前插入 HTML 换行标记
my_echo( number_format( 12345678 ) );
echo nl2br("1
2
3
4");

// str_pad — 补全
my_echo(  str_pad( 'Easy' , 20 , "🍩" ) );

// str_repeat — 重复一个字符串
my_echo(  $🍩s = str_repeat( "🍩" , 10 ) );

// str_replace — 子字符串替换
my_echo( str_replace( "🍩" , "🍖" , $🍩s ) );
my_echo( str_replace( "🍩" , "🍖" , $🍩s , $count ) );
my_echo( $count );

// substr_replace — 替换字符串的子串
my_echo( substr_replace( $🍩s , "🍖" , 0 , 4 ) ); // emoji是4个字节

// strlen — 获取字符串长度
my_echo( strlen( "🍖" ) );
my_echo( mb_strlen( "🍖" , "UTF8" ) );

$dinner = "🍚🍖🍖🍚🍷";

// strpos — 查找字符串首次出现的位置
my_echo( strpos( $dinner , "🍖" ) );
my_echo( mb_strpos( $dinner , "🍖" , 0 , "UTF8" ) );
my_echo( mb_strpos( $dinner , "🍚" , 0 , "UTF8" ) );
my_echo( mb_strpos( $dinner , "🍷" , 0 , "UTF8" ) );

// stripos — 查找字符串首次出现的位置（不区分大小写）

// strstr — 查找字符串的首次出现，返回首次出现，并到结束的字符串
my_echo( mb_strstr( $dinner , "🍖" , false , "UTF8" ) );

// stristr — strstr 函数的忽略大小写版本
// strrchr — 查找指定字符在字符串中的最后一次出现
my_echo( strrchr( $dinner , "🍚" ) );
my_echo( mb_strrchr( $dinner , "🍚" , false , "UTF8" ) );

// strripos — 计算指定字符串在目标字符串中最后一次出现的位置（不区分大小写）
// strrpos — 计算指定字符串在目标字符串中最后一次出现的位置
my_echo( mb_strrpos( $dinner , "🍖" , 0 , "UTF8" ) );



// substr_count — 计算字串出现的次数
my_echo( mb_substr_count( $dinner , "🍖" , "UTF8" ) );

// substr — 返回字符串的子串
my_echo( mb_substr( $dinner , 0 , 3 ,  "UTF8" ) );

// 更为复杂的字符串操作，可以使用正则表达式来做。正则表达式学起来比较难，日常开发用到的时候又比较少，所以这里我们先不讲，把它留到面试串讲的时候来讲。