<?php
// 错误类和异常类

// 异常机制，是错误代码重用的一种方式。

/*

// 在异常机制之前的错误处理

$data = get_data( "SHOW TABLES" ) or die("查询数据库失败");

run_sql( $sql );
if( db_errno() !== 0 )
{
    die( "数据库错误 - " + db_error() );
}

// 注意这里是每一次查询都要如此处理

*/


// 有了异常机制以后

try
{
    $a = 100;
    $a->bbc();
    //$value = 1 % 0;
}
catch( DivisionByZeroError $e )
{
    my_dump( $e );
}
catch( Throwable $t ) // 注意这里可以多次 catch，这样我们就可以有优先级、有层次的处理异常
{
    my_dump( $t );
}
finally
{
    // 这里是 catch 不 catch 都会执行的代码
}

// 通过嵌套，每一个层次，可以处理自己层次的异常
try
{
    
    function div()
    {
        try
        {
            $a = 100;
            $a->bbc();
        }
        catch( DivisionByZeroError $e )
        {
            my_dump( $e );
        }
    }

    div();
    
}catch( Throwable $t )
{
    my_dump( $t );
}
finally
{
    // 这里是 catch 不 catch 都会执行的代码
}

// 手工控制错误和异常

function error_handler( $errno, $errstr, $errfile, $errline ) {
    throw new Error( $errstr, 0, $errno, $errfile, $errline );
}
set_error_handler("error_handler");

// 1/0;

// set_exception_handler("")