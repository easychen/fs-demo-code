<?php
// 变量作用域demo

$best_langauge = 'PHP';
unset( $best_langauge );
my_dump( @$best_langauge );

$GLOBALS['best_langauge'] = 'PHP';

// include & require 
require 'demo.part.6.php';

// 函数和 global

function show_best_language()
{
    echo "最好的语言是 " . @$best_langauge . "\r\n";
}

show_best_language();

function show_best_language2()
{
    global $best_langauge;
    echo "最好的语言是 " . $best_langauge. "\r\n";
}

show_best_language2();

// 静态变量
function up()
{
    static $time = 0;
    echo ++$time."\r\n";
}

up();
up();
up();

// 这个在写递归的时候非常好用


// 变量的变量
// 变量名也可以是变量
$type = 'best_langauge';
my_echo( $$type );
my_echo( ${$type} );



