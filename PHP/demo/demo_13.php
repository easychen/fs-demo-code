<?php
// trait

class Neko
{
    public function paw()
    {
        echo "🐾";
    }
}

class Girl
{
    public function talk()
    {
        echo "今天天气真好~";
    }
}

// class NekoGirl extends  Neko , Girl {}  // <-- wrong!

trait  paw
{
    public function paw()
    {
        echo "🐾";
    }
} 

trait  talk
{
    public function talk()
    {
        echo "今天天气真好~";
    }
} 

class NekoV2
{
    use paw;
}

class GirlV2
{
    use talk;
}

class NekoGirl
{
    use paw;
    use talk;
}

$🐱 = new NekoGirl();
$🐱 ->paw();
$🐱 ->talk();

class NekoGirlV2
{
    public $id = 0;
    use paw , talk ;

    public function talk()
    {
        echo "主人今天天气真好喵~";
    } 
}

$🐱 = new NekoGirlV2();
$🐱 ->paw();
$🐱 ->talk();
// insteadof 解决冲突 ， as 别名

$🐱->id = 1;


// clone =========================

$🐱2 = clone $🐱;
$🐱2->paw();
$🐱2->id = 2;

// 对象赋值，默认传递的是引用
$🐱3 = $🐱;
$🐱3->id = 3;

echo "\r\n\r\n";

my_echo( $🐱->id ) ;
my_echo( $🐱2->id ) ;
my_echo( $🐱3->id ) ;

// 序列化 =========================
$data = serialize( $🐱 );
$🐱4 = unserialize( $data );
$🐱->id = 1;
my_echo( $🐱4->id ) ; 

