<?php
// 服务器端信息
// 可以在 nginx 配置中指定
my_echo( $_SERVER );

// 可以在 apache 配置中指定
my_echo( $_ENV );

my_echo( $_GET );
my_echo( $_POST );
my_echo( $_REQUEST );
// 同名覆盖问题 http://php.net/manual/zh/ini.core.php#ini.variables-order
my_echo( $_FILES );

setcookie( 'cookie_username' , '@Easy' );
my_echo( $_COOKIE );

$_SESSION['session_username'] = '@Easy';
my_echo( $_SESSION );


