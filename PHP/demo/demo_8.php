<?php
// 运算符

// ① 次方 ============== 
$a = 2;
my_echo( $a ** 10 );

// ② 引用赋值 ===========
$b = &$a; // 引用，引用可以理解为别名
my_echo( $b );

// 对别名的修改，一样会影响到原变量
$b = 3;
my_echo( $a ); 

// 对别名的删除不会影响原变量，因为 PHP 采用引用计数模式，只有一个变量没有其他别名时，才会被删掉
unset( $b );
my_echo( $a ); 

$c = &$a;
unset( $a );
my_echo( $c ); 


// ③ 比较运算符 ==============

// 太空船运算符
my_echo( 1 <=> 1 );
my_echo( 1 <=> 2 );
my_echo( 2 <=> 1 );

// null 合并运算符
// 从左往右取第一个不为 null 的值
my_echo( $a ?? $b ?? $c ?? $d );

// ④ 错误控制运算符 ==============
print_r( $data['book'] );
print_r( @$data['book'] );

// ⑤ 逻辑运算符 ==============
if( true and false or true ) echo "PHP 除了 && 和 || 还支持 and 和 or \r\n\r\n";

// ⑥ 运行运算符 ==============
echo `php -v`; 

// 运行运算符直接调用命令行命令，可以作为 PHP 和其他程序的接口。
// 但出于安全考虑，很多 PHP 运行环境禁掉了它。


// ⑦ 数组运算符 ==============
$city = [ 1=>'beijing' , 2=>'shanghai' , 3=>'shenzhen' ];
$city2 = [ 1=>'chongqing' , 2=>'chengdu' ];
$city3 = [ 2=>'shanghai' , 1=>'beijing' , 3=>'shenzhen' ];

my_echo( $city + $city2 ); // 加号更类似于追加，如果 key 已经有值了，不会覆盖
my_echo( $city2 + $city );
my_dump( $city == $city3 );
my_dump( $city === $city3 ); // 三个等号时，顺序也要一致

// ⑧ 类型运算符 ==============

class Dog{};
class Cat{};

$🐶 = new Dog();

my_dump( $🐶 instanceof Dog  );
my_dump( $🐶 instanceof Cat  );







