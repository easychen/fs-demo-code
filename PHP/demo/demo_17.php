<?php
// 数组

/*
array — 新建一个数组
// array 不是函数，而是类似函数的语法结构
*/
$dogs = Array( "1" => "🐶" , "2" => "🐶" , "3" => "🐶"  ); 

my_echo( $dogs );

$cats = [ "🐱" , "🐱" , "🐱" ];
$cats2d = [ 
            ["🐱","🐱","🐱","🐱","🐱","🐱"],
            ["🐱","🐱","🐱","🐱","🐱","🐱"],
            ["🐱","🐱","🐱","🐱","🐱","🐱"],
            ["🐱","🐱","🐱","🐱","🐱","🐱"],
            ["🐱","🐱","🐱","🐱","🐱","🐱"],
            ["🐱","🐱","🐱","🐱","🐱","🐱"]
          ];  

my_echo( $cats2d );


/*
array_pop — 弹出数组最后一个单元（出栈）
array_push — 将一个或多个单元压入数组的末尾（入栈）
array_shift — 将数组开头的单元移出数组
array_unshift — 在数组开头插入一个或多个单元
*/

array_push( $dogs , "🐮" );
my_echo( $dogs );
$ox = array_pop( $dogs );
my_echo( $dogs );

array_unshift( $dogs , "🐮" );
my_echo( $dogs );
$ox = array_shift( $dogs);
my_echo( $dogs );

/*
array_slice — 从数组中复制出一段
array_splice — 去掉数组中的某一部分并用其它值取代
array_merge_recursive — 递归地合并一个或多个数组
array_merge — 合并一个或多个数组
array_unique — 移除数组中重复的值
array_reverse — 返回单元顺序相反的数组
*/
$cut = array_slice( $dogs , 1 , 1 );
my_echo( $cut );
array_splice( $dogs , 0 , 2 , ["🐕","🐕"] );
my_echo( $dogs );

$animal = array_merge( $dogs , $cats );
my_echo( $animal );



/*
array_values — 返回数组中所有的值
array_key_exists — 检查数组里是否有指定的键名或索引
array_keys — 返回数组中部分的或所有的键名
array_change_key_case — 将数组中的所有键名修改为全大写或小写
key_exists — 别名 array_key_exists
*/
$animal['frog'] = "🐸";
$keys = array_keys( $animal  );
my_echo( $keys );

if( array_key_exists( 'frog' , $animal ) ) 
    my_echo( "frog need a bug 🐸" );

// array_chunk — 将一个数组分割成多个
// array_fill_keys — 使用指定的键和值填充数组
// array_fill — 用给定的值填充数组
$bugs = array_fill( 0 , 10 , "🐛" );
$bugs2d = array_chunk( $bugs , 2 );

my_echo( $bugs2d );

// 将每一行第一个元素换掉
// array_map — 为数组的每个元素应用回调函数
// array_walk — 使用用户自定义函数对数组中的每个元素做回调处理
$bugs2dV2 = array_map( function( $item ){
        $item[0] = "🐞";
        return $item;
    } , $bugs2d );

array_walk( $bugs2d , function( &$item ){
    $item[0] = "🐞";
    return $item;
} );   

my_echo( $bugs2dV2 );
my_echo( $bugs2d );


// array_column — 返回数组中指定的一列
$first_column = array_column( $bugs2dV2 , 0 );
my_echo( $first_column );

// array_pad — 以指定长度将一个值填充进数组
$first_columnV2 = array_pad( $first_column , 10 , "🐛" );
my_echo( $first_columnV2 );

//array_filter — 用回调函数过滤数组中的单元
my_echo(  array_filter( $first_columnV2 , function( $item ){
    if( $item == "🐛" ) return false;
    else return true;

} ));


//array_flip — 交换数组中的键和值
my_echo( array_flip( $first_columnV2 ) );

/*
array_product — 计算数组中所有值的乘积
array_sum — 对数组中所有值求和
array_reduce — 用回调函数迭代地将数组简化为单一的值
array_rand — 从数组中随机取出一个或多个单元
*/

$numbers = [ 1 , 5 , 23 , 45 , 56 ];
my_echo( array_product( $numbers ) );
my_echo( array_sum( $numbers ) );
my_echo( array_rand( $numbers , 2 ) ); // 返回的是 Key



$php_tech_keywords = [
    "php"=>1266,
    "经验"=>1164,
    "mysql"=>1049,
    "能力"=>922,
    "框架"=>888,
    "数据库"=>804,
    "项目"=>773,
    "css"=>651,
    "代码"=>641,
    "html"=>636,
    "linux"=>612,
    "产品"=>592,
    "javascript"=>574,
    "网站"=>533,
    "学历"=>524,
    "web"=>521,
    "语言"=>505,
    "文档"=>445
    ];   
/*
array_replace — 使用传递的数组替换第一个数组的元素
array_replace_recursive — 使用传递的数组递归替换第一个数组的元素
*/
// 不存在的 Key 会自己建一个
my_echo( array_replace( $php_tech_keywords , [ "网站"=>555 , "不存在"=>999 ] ));


// array_search — 在数组中搜索给定的值，如果成功则返回首个相应的键名
my_echo( array_search( 505 , $php_tech_keywords ) );
my_dump( in_array( 505 , $php_tech_keywords ) );



/*
sort — 对数组排序
rsort — 对数组逆向排序
ksort — 对数组按照键名排序
krsort — 对数组按照键名逆向排序

uasort — 使用用户自定义的比较函数对数组中的值进行排序并保持索引关联
uksort — 使用用户自定义的比较函数对数组中的键名进行排序
usort — 使用用户自定义的比较函数对数组中的值进行排序
arsort — 对数组进行逆向排序并保持索引关系
asort — 对数组进行排序并保持索引关系

array_multisort — 对多个数组或多维数组进行排序
natcasesort — 用“自然排序”算法对数组进行不区分大小写字母的排序
natsort — 用“自然排序”算法对数组排序
*/
$php_tech_keywordsV2 = $php_tech_keywords;
shuffle( $php_tech_keywordsV2 );
my_echo( $php_tech_keywordsV2 );

sort( $php_tech_keywordsV2 );
my_echo( $php_tech_keywordsV2 );
rsort( $php_tech_keywordsV2 );
my_echo( $php_tech_keywordsV2 );

ksort( $php_tech_keywords );
my_echo( $php_tech_keywords );
krsort( $php_tech_keywords );
my_echo( $php_tech_keywords );

// count — 计算数组中的单元数目，或对象中的属性个数
// sizeof — count 的别名
my_echo( '数组 $php_tech_keywords 有'. count( $php_tech_keywords ) . '个元素' );

/*
current — 返回数组中的当前单元
each — 返回数组中当前的键／值对并将数组指针向前移动一步
reset — 将数组的内部指针指向第一个单元
end — 将数组的内部指针指向最后一个单元
next — 将数组中的内部指针向前移动一位
pos — current 的别名
prev — 将数组的内部指针倒回一位
key — 从关联数组中取得键名
*/
reset( $php_tech_keywords );
my_echo( current( $php_tech_keywords ) );
next( $php_tech_keywords );
my_echo( current( $php_tech_keywords ) );

while( $now = each( $php_tech_keywords ) )
{
    my_echo( $now );
}

// compact — 建立一个数组，包括变量名和它们的值
$theone = '255';
$thetwo = '122';
my_echo( $allinone = compact( "theone" , "thetwo" )  );


// extract — 从数组中将变量导入到当前的符号表
$data = [ 'thetitle'=>'方糖气球' , 'theurl'=>'http://ftqq.com' ];
extract( $data );

my_echo( $thetitle );
my_echo( $theurl );


// list — 把数组中的值赋给一组变量
// list 不是函数，而是类似函数的语法结构
list( $title , $url ) = [ "title" , "url" ];

my_echo( $title );
my_echo( $url );

list(  , $url2 ) = [ "title" , "url" ];
my_echo( $url2 );