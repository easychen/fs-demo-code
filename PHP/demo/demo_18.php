<?php
// 日期时间
// 日期时间首先是和时区相关的
// 中国所在地区为东八区
@date_default_timezone_set('Asia/Chongqing');
// setlocale(LC_ALL, 'zh_CN');

my_echo( date("Y-m-d H:i:s") ); 
// 格式查看 http://php.net/manual/zh/function.date.php

my_echo('今天是' . date("Y") . '年，第' . date("z") . '天，第' . date("W") .'周'.date("l"));

// date 接受第二个参数，时间戳
// time 返回当前的时间戳，时间戳是 1970年到现在发生的秒数
my_echo( date("Y-m-d H:i:s" , time() - 60*60*24 ) );

// strtotime
my_echo( date("Y-m-d H:i:s" , strtotime( "last monday" ) ));
my_echo( date("Y-m-d H:i:s" , strtotime( "next monday" ) ));

my_echo( date("Y-m-d H:i:s" , strtotime( "+23 hours" ) ));
my_echo( date("Y-m-d H:i:s" , strtotime( "-4 days" ) ));

my_echo( date("Y-m-d H:i:s" , strtotime( "2018/1/23" ) ));
my_echo( date("Y-m-d H:i:s" , strtotime( "2018-1-23" ) ));
my_echo( date("Y-m-d H:i:s" , strtotime( "now" ) ));
