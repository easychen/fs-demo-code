<?php
// 控制结构

// ① elseif ==================
$feel_good = false;
$product_expired = true;

if( $feel_good  )
    $price = 200;
else
    if( $product_expired )
        $price = 201;
    else
        $price = 300;    
        
my_echo( $price );  

$product_expired = false;

if( $feel_good  )
    $price = 200;
elseif( $product_expired )
    $price = 201;
else
    $price = 300;
    
my_echo( $price );     
        
// ② foreach ==================       
foreach( $demos as $key => $value )
{
    // key = 3 以后， 不打印内容
    // if( $key == 3 ) break;

    // key = 3 时， 不打印内容
    // if( $key == 3 ) continue;

    echo "KEY = $key , VALUE = $value \r\n";
    
}

// ③ include & require ================== 

// include 只会造成警告
include 'nofile';

// require 会造成fatal error（终止）
// require 'nofile';

// 重复加载可能导致问题
// require( '_function.php' );
// require_once( '_function.php' );

// return  可以返回值
$number =  require 'demo.part.9.php';
echo $number;

// return 以后的函数定义、类定义依然有效
// 看看 _function.php 
$jump = 0;

back:
    echo "time";
    if( ++$jump < 5 ) goto back;
